#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState
from std_srvs.srv import Trigger, TriggerResponse
import time
import math

class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z

class GameController:

    def __init__(self, robot_name):

        # hard coded numbers
        self.ring_radius = 0.33 # fix me - why not matching the dae file?
        self.total_ring_count = 64
        self.ring_positions = [
            Vector3D(-8.8534, -5.9629, 2.6756),
            Vector3D(-7.8112, -6.0292, 2.6763),
            Vector3D(-6.7705, -6.1064, 2.6766),
            Vector3D(-5.7306, -6.1894, 2.6765),
            Vector3D(-4.6911, -6.2768, 2.6760),
            Vector3D(-3.6520, -6.3681, 2.6747),
            Vector3D(-2.6133, -6.4640, 2.6724),
            Vector3D(-1.5756, -6.5646, 2.6684),
            Vector3D(-0.5409, -6.6424, 2.6654),
            Vector3D(0.4707, -6.5600, 2.6805),
            Vector3D(1.3853, -6.1239, 2.7374),
            Vector3D(2.3299, -5.6751, 2.7586),
            Vector3D(3.3280, -5.9001, 2.6012),
            Vector3D(4.2099, -6.3712, 2.3650),
            Vector3D(5.1713, -6.4999, 2.1415),
            Vector3D(6.1362, -6.2024, 1.9764),
            Vector3D(7.0766, -5.7777, 1.8376),
            Vector3D(8.0314, -5.3703, 1.6977),
            Vector3D(9.0259, -5.0693, 1.5400),
            Vector3D(10.0498, -4.9201, 1.3609),
            Vector3D(11.0772, -4.8607, 1.1736),
            Vector3D(12.0968, -4.7864, 0.9956),
            Vector3D(12.9505, -4.3711, 0.9068),
            Vector3D(13.0417, -3.4097, 1.0416),
            Vector3D(12.7941, -2.4233, 1.2302),
            Vector3D(12.5328, -1.4310, 1.4016),
            Vector3D(12.3885, -0.4078, 1.5223),
            Vector3D(12.5659, 0.5989, 1.5126),
            Vector3D(12.8498, 1.4873, 1.2603),
            Vector3D(12.6856, 2.3078, 0.8272),
            Vector3D(11.8880, 2.7673, 0.6174),
            Vector3D(10.8877, 2.7690, 0.7081),
            Vector3D(9.9230, 2.5185, 0.8693),
            Vector3D(9.0342, 2.0692, 1.0206),
            Vector3D(8.1882, 1.4882, 1.1386),
            Vector3D(7.2551, 0.9709, 1.2321),
            Vector3D(6.1541, 0.9380, 1.2977),
            Vector3D(5.2130, 1.4612, 1.3247),
            Vector3D(4.3673, 2.0624, 1.3344),
            Vector3D(3.4830, 2.5134, 1.3364),
            Vector3D(2.5505, 2.4792, 1.3355),
            Vector3D(1.6708, 2.0036, 1.3345),
            Vector3D(0.8288, 1.3938, 1.3336),
            Vector3D(-0.1056, 0.8572, 1.3325),
            Vector3D(-1.2161, 0.8789, 1.3368),
            Vector3D(-2.1267, 1.4761, 1.3769),
            Vector3D(-2.9127, 2.1681, 1.4930),
            Vector3D(-3.7474, 2.7323, 1.7407),
            Vector3D(-4.6354, 3.1045, 2.1235),
            Vector3D(-5.5335, 3.2735, 2.6104),
            Vector3D(-6.3975, 3.2204, 3.1706),
            Vector3D(-7.1954, 2.9366, 3.7446),
            Vector3D(-7.9199, 2.4457, 4.2616),
            Vector3D(-8.5844, 1.8001, 4.6702),
            Vector3D(-9.3313, 0.9052, 4.9769),
            Vector3D(-9.9357, 0.0974, 5.0807),
            Vector3D(-10.4929, -0.7495, 5.0468),
            Vector3D(-10.9675, -1.6267, 4.8720),
            Vector3D(-11.3195, -2.5200, 4.5550),
            Vector3D(-11.5226, -3.4123, 4.1106),
            Vector3D(-11.5904, -4.2920, 3.5856),
            Vector3D(-11.5016, -5.1570, 3.0283),
            Vector3D(-10.9144, -5.8357, 2.6693),
            Vector3D(-9.8973, -5.9180, 2.6734)
        ]

        self.ring_normals = [
            Vector3D(0.9987, -0.0516, -0.0008),
            Vector3D(0.9974, -0.0715, -0.0008),
            Vector3D(0.9974, -0.0715, -0.0008),
            Vector3D(0.9967, -0.0815, -0.0008),
            Vector3D(0.9967, -0.0815, -0.0008),
            Vector3D(0.9958, -0.0915, -0.0008),
            Vector3D(0.9958, -0.0915, -0.0024),
            Vector3D(0.9958, -0.0915, -0.0024),
            Vector3D(0.9995, -0.0316, -0.0008),
            Vector3D(0.9656, 0.2569, 0.0408),
            Vector3D(0.8561, 0.5132, 0.0608),
            Vector3D(0.9789, 0.1984, -0.0492),
            Vector3D(0.8571, -0.4700, -0.2108),
            Vector3D(0.9049, -0.3526, -0.2385),
            Vector3D(0.9739, 0.1273, -0.1881),
            Vector3D(0.9160, 0.3765, -0.1388),
            Vector3D(0.9014, 0.4134, -0.1288),
            Vector3D(0.9268, 0.3489, -0.1388),
            Vector3D(0.9636, 0.2155, -0.1585),
            Vector3D(0.9803, 0.0885, -0.1767),
            Vector3D(0.9828, 0.0492, -0.1782),
            Vector3D(0.9765, 0.1476, -0.1570),
            Vector3D(0.5314, 0.8461, 0.0408),
            Vector3D(-0.1657, 0.9697, 0.1798),
            Vector3D(-0.2711, 0.9456, 0.1798),
            Vector3D(-0.2150, 0.9650, 0.1502),
            Vector3D(-0.0192, 0.9972, 0.0723),
            Vector3D(0.3319, 0.9360, -0.1173),
            Vector3D(0.1287, 0.9084, -0.3979),
            Vector3D(-0.5207, 0.7463, -0.4147),
            Vector3D(-0.9835, 0.1806, -0.0076),
            Vector3D(-0.9794, -0.1380, 0.1471),
            Vector3D(-0.9228, -0.3473, 0.1668),
            Vector3D(-0.8402, -0.5268, 0.1288),
            Vector3D(-0.8213, -0.5619, 0.0990),
            Vector3D(-0.9399, -0.3325, 0.0775),
            Vector3D(-0.9511, 0.3063, 0.0408),
            Vector3D(-0.8187, 0.5741, 0.0124),
            Vector3D(-0.8356, 0.5494, 0.0024),
            Vector3D(-0.9633, 0.2683, 0.0008),
            Vector3D(-0.9460, -0.3240, -0.0008),
            Vector3D(-0.8253, -0.5646, -0.0008),
            Vector3D(-0.8139, -0.5810, -0.0008),
            Vector3D(-0.9460, -0.3240, -0.0008),
            Vector3D(-0.9317, 0.3631, 0.0124),
            Vector3D(-0.7623, 0.6442, 0.0623),
            Vector3D(-0.7653, 0.6209, 0.1700),
            Vector3D(-0.8389, 0.4492, 0.3073),
            Vector3D(-0.8662, 0.2600, 0.4267),
            Vector3D(-0.8551, 0.0613, 0.5148),
            Vector3D(-0.8102, -0.1642, 0.5627),
            Vector3D(-0.7431, -0.3869, 0.5460),
            Vector3D(-0.6787, -0.5717, 0.4611),
            Vector3D(-0.6345, -0.6938, 0.3407),
            Vector3D(-0.6050, -0.7782, 0.1684),
            Vector3D(-0.5731, -0.8186, 0.0376),
            Vector3D(-0.5121, -0.8528, -0.1022),
            Vector3D(-0.4142, -0.8780, -0.2400),
            Vector3D(-0.2739, -0.8831, -0.3809),
            Vector3D(-0.1318, -0.8672, -0.4801),
            Vector3D(-0.0007, -0.8460, -0.5332),
            Vector3D(0.2184, -0.8236, -0.5234),
            Vector3D(0.9593, -0.2775, -0.0524),
            Vector3D(0.9995, -0.0316, 0.0076)
        ]
        self.robot_name = robot_name
        self.robot_status = "Paused"

        self.crossed_ring_count = 0
        self.out_of_sequence = False
        self.last_ring_index = -1  # because next will be 0
        self.time_passed = 0.0
        self.start_time = 0.0

        self.metrics = {
            'Robot Name' : self.robot_name,
            'Robot Status' : self.robot_status,
            'Ring Counter' : self.crossed_ring_count,
            'Time' : self.time_passed
        }

        rospy.init_node("game_controller")

        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        self.ring_is_crossed = [False] * self.total_ring_count

        self.init_services()

        while ~rospy.is_shutdown():
            if self.robot_status == "Running" and self.crossed_ring_count < self.total_ring_count:
                self.time_passed = rospy.get_time() - self.start_time

            self.ring_cross_check()
            self.publish_metrics()

            rate.sleep()
            if self.robot_status == "Stop":
                break


    def init_services(self):
        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.get_robot_state("quadrotor", "")
            if not resp.success == True:
                self.robot_status = "no_robot"
            else:
                self.robot_new_position = Vector3D(resp.pose.position.x, resp.pose.position.y, resp.pose.position.z)


        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "no_robot"
            rospy.logerr("Service call failed: %s" % (e,))



    def handle_robot(self,req):
        if self.robot_status == "Paused":
            self.robot_status = "Running"
            self.start_time = rospy.get_time()

            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Running":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

    def publish_metrics(self):
        self.metrics['Robot Status'] = self.robot_status
        self.metrics['Ring Counter'] = self.crossed_ring_count

        # round will make it show as an int (39.0000) for example - need decimal precision
        self.metrics['Time'] = self.time_passed # round(self.time_passed, 2)

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def ring_cross_check(self):
        resp = self.get_robot_state("quadrotor", "")

        # save old position and get new position - this gives us segment of the drone movement
        self.robot_prev_position = self.robot_new_position
        self.robot_new_position = Vector3D(resp.pose.position.x, resp.pose.position.y, resp.pose.position.z)

        a = self.robot_prev_position
        b = self.robot_new_position

        for i in range (self.total_ring_count):
            if not self.ring_is_crossed[i]:
                p = self.ring_positions[i]   # point in center of ring
                n = self.ring_normals[i]   # normal of ring
                d = b - a # drone move segment
                margin = max(abs(d.x), max(d.y, d.z)) # quick reject bounding box extended to capture segment crossing the corner
                l = self.ring_radius + margin # half length of quick reject bounding box
                # quick reject of bounding box is efficient - avoid dot products before this point
                if a.x < p.x + l and a.x > p.x - l and a.y < p.y + l and a.y > p.y - l and a.z < p.z + l and a.z > p.z - l:
                    a2 = a - p
                    b2 = b - p
                    dot_a = a2.dot(n)
                    dot_b = b2.dot(n)
                    if dot_a * dot_b <= 0.0:
                   # if dot_a < 0 and dot_b >= 0: # true if segment crosses or touches plane in the proper direction
                        dot_d = d.dot(n)
                        if dot_d < 1e-6: # unlikely case - parallel to the plane and touching - this might happen from a bounce
                            intersect = a # just use a as intersect - either a and b are both in ring or both out
                        else:
                            dot_ratio = dot_a / dot_d # fraction of the drone movement segment we need to arrive at intersect on plane
                            intersect = a - d * dot_ratio # the intersection point
                        di = intersect - p
                        rSquared = di.x * di.x + di.y * di.y + di.z * di.z # do not use sqrt
                        if rSquared < self.ring_radius * self.ring_radius:
                            if dot_a < 0 and dot_b >= 0:
                                if self.last_ring_index + 1 == i:
                                    print 'forward', i
                                    self.ring_is_crossed[i] = True
                                    self.last_ring_index = i
                                    self.crossed_ring_count += 1
                                else:
                                    print 'out of sequence from ', self.last_ring_index, ' to ', i
                                    self.out_of_sequence = True
                            else:
                                print 'backward', i

if __name__ == '__main__':
    # Name of robot
    controller = GameController("Quadrotor")
